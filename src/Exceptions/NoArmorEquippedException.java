package Exceptions;

public class NoArmorEquippedException extends Exception{
    public NoArmorEquippedException(String s) {
        super(s);
    }
}

