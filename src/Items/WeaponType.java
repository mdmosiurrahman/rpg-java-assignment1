package Items;

/**
 * Types of existing weapons
 */
public enum WeaponType {
    AXE, BOW, DAGGER, HAMMER, STAFF, SWORD, WAND
}
