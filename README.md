### RPG Game Character

## Description

Simple text based program that can create different types of characters that are of different classes. I.e. Warrior, Mage, Rogue and Ranger. Each character can level up and increase their stats, equip created weapon or armor.
This is the Java Assignment 5 in the Experis Academy program.

## Technologies use
- Java console application built with JDK16 and Junit5 was used for unit tests.
- Use OOP concepts.

### Run the program
A demonstration for the program is in Main class, which can be executed from the jar file.

### Test the program
For testing, the program has a own directory called Tests. This directory has tests for all the class, for many conditions. The testing is using JUnit
